(function pause(){
    var stringsInp = document.getElementById('stringsInput');
    var columnsInp = document.getElementById('columnsInput');
    var titleInp = document.getElementById('titleInput');
    var textInp = document.getElementById('textInput');
    var submitBtn = document.querySelector('.btn-roll')
    var container = document.querySelector('.table-container');

    submitBtn.addEventListener('click', function () {
        container.innerHTML = '';

        container.innerHTML = '<tr id="trr"></tr>'

        for(var s = 0; s < stringsInp.value; s++){
            var tr = document.createElement("tr");
            container.appendChild(tr);

            for(var c = 0;c < columnsInp.value; c++){
                var td = document.createElement("td");
                td.innerHTML = ''+textInp.value+''
                td.className = 'td-tag';
                tr.appendChild(td);
            }
        }

        for(var t = 0;t < columnsInp.value; t++){
            var trr = document.getElementById('trr');
            var th = document.createElement("th");
            th.innerHTML = ''+titleInp.value+''
            th.className = 'th-tag'
            trr.appendChild(th);
        }

        var allTds = document.querySelectorAll('.td-tag')

        for(var i = 0; i < allTds.length; i++) {
            allTds[i].addEventListener('click', function(e){

                var td = e.target;
                var tdValue = td.innerHTML;

                td.innerHTML = ''

                var input = document.createElement('input');
                input.className = "input-change"
                input.value = tdValue;
                td.appendChild(input);
                input.focus();

                input.addEventListener('blur', function () {
                    var allTds = document.querySelectorAll('.td-tag')
                    for(var k = 0; k < allTds.length; k++) {
                        td.className = "td-tag"
                        allTds[k].innerHTML = input.value;
                    }
                })


            });
        }
        var allThs = document.querySelectorAll('.th-tag')
        for(var h = 0; h < allThs.length; h++) {
            allThs[h].addEventListener('click', ({target: t}) => {
                if (t.classList.contains('th-tag')) {
                    var result = t.innerHTML.replace(/\d+(?:\.\d+)?/g, m => +m + 1);
                    t.innerHTML = result;
                }
            });
        }
    });
})();
